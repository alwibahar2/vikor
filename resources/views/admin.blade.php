@extends('master')
@section('content')
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
            <!-- Widgets  -->
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-five">
                                <div class="stat-icon dib flat-color-1">
                                    <i class="pe-7s-note2"></i>
                                </div>
                                <div class="stat-content">
                                    <div class="text-left dib">
                                        <div class="stat-text"><span class="count"></span></div>
                                        <div class="stat-heading">Total Gejala</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-five">
                                <div class="stat-icon dib flat-color-2">
                                    <i class="pe-7s-note2"></i>
                                </div>
                                <div class="stat-content">
                                    <div class="text-left dib">
                                        <div class="stat-text"><span class="count"></span></div>
                                        <div class="stat-heading">Total Penyakit</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-five">
                                <div class="stat-icon dib flat-color-2">
                                    <i class="pe-7s-users"></i>
                                </div>
                                <div class="stat-content">
                                    <div class="text-left dib">
                                        <div class="stat-text"><span class="count"></span></div>
                                        <div class="stat-heading">Total Pasien</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-five">
                                <div class="stat-icon dib flat-color-2">
                                    <i class="pe-7s-display2"></i>
                                </div>
                                <div class="stat-content">
                                    <div class="text-left dib">
                                        <div class="stat-text"><span class="count"></span></div>
                                        <div class="stat-heading">Total Hasil Diagnosa</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Widgets -->
            <!--  /Traffic -->
            <div class="clearfix"></div>
            <!-- Orders -->
            <div class="orders">
                <div class="row">
                    <div class="col-xl-8">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="box-title">Hasil Diagnosa</h4>
                            </div>
                            <div class="card-body--">
                                <div class="table-stats order-table ov-h">
                                    <table class="table ">
                                        <thead>
                                        <tr>
                                            <th class="serial">#</th>
                                            <th>Nama</th>
                                            <th>Umur</th>
                                            <th>Hasil Diagnosa</th>
                                            <th>Tanggal Pemeriksaan</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                           <tr></tr>
                                           <tr></tr>
                                           <tr></tr>
                                           <tr></tr>
                                           <tr></tr>
                                        </tbody>
                                    </table>
                                </div> <!-- /.table-stats -->
                            </div>
                        </div> <!-- /.card -->
                    </div>  <!-- /.col-lg-8 -->

{{--                    <div class="col-xl-4">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-lg-6 col-xl-12">--}}
{{--                                <div class="card br-0">--}}
{{--                                    <div class="card-body">--}}
{{--                                        <div class="chart-container ov-h">--}}
{{--                                            <div id="flotPie1" class="float-chart"></div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div><!-- /.card -->--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div> <!-- /.col-md-4 -->--}}
                </div>
            </div>
            <!-- /.orders -->
            <!-- /#add-category -->
        </div>
        <!-- .animated -->
    </div>
@endsection
