<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">
</head>
<style>
    body {
        font-family: "Lato", sans-serif;
    }



    .main-head{
        height: 150px;
        background: #FFF;

    }

    .sidenav {
        height: 100%;
        background-color: #6c8cbf;
        overflow-x: hidden;
        padding-top: 20px;
    }


    .main {
        padding: 0px 10px;
    }

    @media screen and (max-height: 450px) {
        .sidenav {padding-top: 15px;}
    }

    @media screen and (max-width: 450px) {
        .login-form{
            margin-top: 10%;
        }

        .register-form{
            margin-top: 10%;
        }
    }

    @media screen and (min-width: 768px){
        .main{
            margin-left: 40%;
        }

        .sidenav{
            width: 40%;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
        }

        .login-form{
            margin-top: 25%;
        }

        .register-form{
            margin-top: 20%;
        }
    }


    .login-main-text{
        margin-top: 20%;
        padding: 60px;
        color: #fff;
    }

    .login-main-text h2{
        font-weight: 300;
    }

    .btn-black{
        background-color: #000 !important;
        color: #fff;
    }
    .login {
        margin: 250px auto;
        width: 400px;
        /* padding: 10px; */
        /* border: 1px solid #ccc; */
        /* background: lightblue; */
    }
</style>
<body>

<div class="main ">
<div class="card">
                        <div class="card-header">Silahkan login</div>
                        <div class="card-body card-block">
    <div class="col-md-6 col-sm-12">
        <div class="login-form">
            @if(\Illuminate\Support\Facades\Session::has('failed'))
                <div class="alert bg-danger">
                    <b style="color: white">Silahkan Login Terlebih Dahulu</b>
                </div>

            @endif
            @if(\Illuminate\Support\Facades\Session::has('gagal'))
                <div class="alert bg-danger">
                    <b style="color: white">Gagal Melakukan Pendaftaran</b>
                </div>
            @endif
            <form method="post" action="{{route('store_register')}}">
                @csrf
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Nama Orang Tua</label>
                    <input type="text" name="nama_orgtua" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" name="tgl_lhr" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" name="tempat_lhr" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea name="alamat" class="form-control" cols="30" rows="10" required>

                    </textarea>
                </div>
                <button type="submit" class="btn" style="background: #6c8cbf;color: white">register</button>
                <p>Sudah Punya Akun? <b><a href="{{route('login')}}">Login Sekarang</a></b></p>
            </form>
        </div>
    </div>
</div> </div> </div>
</body>

</html>
end document -->


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">
</head>
<style>
    body {
        font-family: "Lato", sans-serif;
    }



    .main-head{
        height: 150px;
        background: #FFF;

    }

    .sidenav {
        height: 100%;
        background-color: #6c8cbf;
        overflow-x: hidden;
        padding-top: 20px;
    }


    .main {
        padding: 0px 10px;
    }
    .login {
        margin: 100px auto;
        width: 400px;
        /* padding: 10px; */
        /* border: 1px solid #ccc; */
        /* background: lightblue; */
    }

    @media screen and (max-height: 450px) {
        .sidenav {padding-top: 15px;}
    }

    @media screen and (max-width: 450px) {
        .login-form{
            margin-top: 10%;
        }

        .register-form{
            margin-top: 10%;
        }
    }

    @media screen and (min-width: 768px){
        .main{
            margin-left: 40%;
        }

        .sidenav{
            width: 40%;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
        }

        .login-form{
            margin-top: 80%;
        }

        .register-form{
            margin-top: 20%;
        }
    }


    .login-main-text{
        margin-top: 20%;
        padding: 60px;
        color: #fff;
    }

    .login-main-text h2{
        font-weight: 300;
    }

    .btn-black{
        background-color: #000 !important;
        color: #fff;
    }
</style>
<body >



<!--                       
<div class="main">
    <div class="col-md-6 col-sm-12">
        <div class="login-form">
            @if(\Illuminate\Support\Facades\Session::has('failed'))
                <div class="alert bg-danger">
                    <b style="color: white">Silahkan Login Terlebih Dahulu</b>
                </div>

            @endif
            @if(\Illuminate\Support\Facades\Session::has('gagal'))
                <div class="alert bg-danger">
                    <b style="color: white">Email/No Telepon atau Password Salah</b>
                </div>

            @endif
                @if(\Illuminate\Support\Facades\Session::has('berhasil'))
                    <div class="alert bg-primary">
                        <b style="color: white">Berhasil Melakukan Pendaftaran</b>
                    </div>

                @endif
            <form method="post" action="{{route('verify')}}">
                @csrf
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" required>
                </div>
                <button type="submit" class="btn" style="background: #6c8cbf;color: white">Login</button>
                <p>Belum Punya Akun? <b><a href="{{route('register')}}">Daftar Sekarang</a></b></p>
            </form>
        </div>
    </div>
</div> -->

<div class="col-lg-6 login">
                    <div class="card">
                        <div class="card-header">Register</div>
                        <div class="card-body card-block">
                        @if(\Illuminate\Support\Facades\Session::has('failed'))
                <div class="alert bg-danger">
                    <b style="color: white">Silahkan Login Terlebih Dahulu</b>
                </div>

            @endif
            @if(\Illuminate\Support\Facades\Session::has('gagal'))
                <div class="alert bg-danger">
                    <b style="color: white">Gagal Melakukan Pendaftaran</b>
                </div>
            @endif
            <form method="post" action="{{route('store_register')}}">
                @csrf
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Nama Orang Tua</label>
                    <input type="text" name="nama_orgtua" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" name="tgl_lhr" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" name="tempat_lhr" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea name="alamat" class="form-control" cols="30" rows="10" required>

                    </textarea>
                </div>
                <button type="submit" class="btn" style="background: #6c8cbf;color: white">register</button>
                <p>Sudah Punya Akun? <b><a href="{{route('login')}}">Login Sekarang</a></b></p>
            </form>
        </div>
    </div>
</div>
</body>

</html>
<!-- end document-->
