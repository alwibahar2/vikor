@extends('master')
@section('content')
    <div class="content">
        <!-- Animated -->
        <div class="animated fadeIn">
            <!-- Widgets  -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-five">
                                <div class="stat-icon dib flat-color-2">
                                    <i class="fa fa-cube"></i>
                                </div>
                                <div class="stat-content">
                                    <div class="text-left dib">
                                        <div class="stat-text"><span class="count">{{$k}}</span></div>
                                        <div class="stat-heading">Data Kriteria</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-five">
                                <div class="stat-icon dib flat-color-2">
                                    <i class="fa fa-cubes"></i>
                                </div>
                                <div class="stat-content">
                                    <div class="text-left dib">
                                        <div class="stat-text"><span class="count">{{$s}}</span></div>
                                        <div class="stat-heading">Data Sub Kriteria</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-five">
                                <div class="stat-icon dib flat-color-2">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="stat-content">
                                    <div class="text-left dib">
                                        <div class="stat-text"><span class="count">{{$al}}</span></div>
                                        <div class="stat-heading">Data Alternatif</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-five">
                                <div class="stat-icon dib flat-color-2">
                                    <i class="fa fa-pencil-square-o"></i>
                                </div>
                                <div class="stat-content">
                                    <div class="text-left dib">
                                        <div class="stat-text"><span class="count">{{$r}}</span></div>
                                        <div class="stat-heading">Data Penilaian</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           
                
                  <!-- /Widgets -->
            <!--  /Traffic -->
   
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="box-title c">Hasil Diagnosa</h4>
                            </div>
                            <div class="card-body--">
                                <div class="table-stats order-table ov-h">
                                    <table class="table ">
                                    <thead>
                                <tr>
                                    <th>no</th>
                                    <th>nama alternatif</th>
                                    <th>nilai Q</th>
                                    <th>ranking</th>
                                </tr>
                                </thead>
                               
                                <tbody>
                                    @for ($a=0;$a<$b;$a++)
                                    
                                    <tr>
                                        <td>{{$a+1}}</td>
                                        <td>{{$rankk[$a]}}</td>
                                        <td>{{round($tes[$a],2)}}</td>
                                        <td>{{$a+1}}</td>
                                    </tr>

                                    @endfor
                                </tbody>
                                    </table>
                                </div> <!-- /.table-stats -->
                            </div>
                        </div> <!-- /.card -->
                    </div>  <!-- /.col-lg-8 -->


        <!-- .animated -->
    </div>
@endsection
