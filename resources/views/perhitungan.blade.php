@extends('master')
@section('content')
    <div class="content">
      
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Nilai alternatif</strong>
                        </div>
                        <div class="card-body">
                           
                            
                            <table id="" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>no</th>
                                    <th>Nama kriteria</th>
                                    @foreach ($kriteria as $k)
                                     <th>{{$k->nama_kriteria}}</th>
                                     @endforeach
                                </tr>
                                </thead>
                               
                                <tbody>
                                @for ($a=0;$a<$b;$a++)
                                <tr>
                                    <td>{{$a+1}}</td>
                                    @foreach ($d[$a] as $key)
                                   
                               
    
                                    <td>{{$key}}</td>
                                    
                                    @endforeach
                                    </tr>
                                @endfor
                                <tr>
                                    <td colspan="2"style="text-align: center;">max</td>
                                    
                                    @foreach ($maxxx as $key)
                                   
                                    <td>{{$key}}</td>
                                    
                                    @endforeach
                                   
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center;" >min</td>
                                    @foreach ($minnn as $key)
                                   
                                   <td>{{$key}}</td>
                                   
                                   @endforeach
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
 
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">matrix normalisasi</strong>
                        </div>
                        <div class="card-body">
                          
                            
                            <table id="" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>no</th>
                                    <th>Nama alternatif</th>
                                    @foreach ($kriteria as $k)
                                     <th>{{$k->nama_kriteria}}</th>
                                     @endforeach
                                </tr>
                                </thead>
                               
                                <tbody>
                                @for ($a=0;$a<$b;$a++)
                                <tr>
                                    <td>{{$a+1}}</td>

                                    <td>{{$relasi[$a]->alternatif->nama_alternatif}}</td>
                                    @foreach ($normalisasi[$a] as $key)
                                   
                               
    
                                    <td>{{$key}}</td>
                                    
                                    @endforeach
                                    </tr>
                                @endfor
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
        
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">nilai W</strong>
                        </div>
                        <div class="card-body">
                          
                            
                            <table id="" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    
                                    <th></th>
                                    @foreach ($kriteria as $k)
                                     <th>{{$k->nama_kriteria}}</th>
                                     @endforeach
                                </tr>
                                </thead>
                               
                                <tbody>
                                    <tr>
                                    <td>bobot kriteria</td>
                                        @foreach($bobot as $k)
                                            <td>{{$k}}</td>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        <td>w</td>
                                        @foreach($w as $k)
                                            <td>{{(round($k,2))}}</td>
                                        @endforeach
                                    </tr>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->

        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">matrix normalisasi terbobot</strong>
                        </div>
                        <div class="card-body">
                         
                            
                            <table id="" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>no</th>
                                    <th>Nama alternatif</th>
                                    @foreach ($kriteria as $k)
                                     <th>{{$k->nama_kriteria}}</th>
                                     @endforeach
                                </tr>
                                </thead>
                               
                                <tbody>
                                    
                                @for ($a=0;$a<$b;$a++)
                                <tr>
                                    <td>{{$a+1}}</td>

                                    <td>{{$relasi[$a]->alternatif->nama_alternatif}}</td>
                                    @foreach ($normalisasi1[$a] as $key)
                                   
                               
    
                                    <td>{{$key}}</td>
                                    
                                    @endforeach
                                    </tr>
                                @endfor
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
      
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">nilai S dan R</strong>
                        </div>
                        <div class="card-body">
                          
                              
                        <table id="" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>no</th>
                                    <th>Nama alternatif</th>
                                    <th>s</th>
                                    <th>r</th>
                                </tr>
                                </thead>
                               
                                <tbody>
                                    
                                @for ($a=0;$a<$b;$a++)
                                <tr>
                                    <td>{{$a+1}}</td>
                                    <td>{{$relasi[$a]->alternatif->nama_alternatif}}</td>
                                    @foreach ($sr[$a] as $key)
                                   
                               
    
                                    <td>{{$key}}</td>
                                    
                                    @endforeach
                                    </tr>
                                    
                                    
                                @endfor
                                <tr>
                                    <td colspan="2"style="text-align: center;">max</td>
                                    @foreach ($srmax as $key)
                                    <td>{{$key}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td colspan="2"style="text-align: center;">min</td>
                                    @foreach ($srmin as $key)
                                    <td>{{$key}}</td>
                                    @endforeach
                                </tr>
                                
                                </tbody>
                            </table>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->

        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Perangkingan</strong>
                        </div>
                        <div class="card-body">
                          
                            
                            <table id="" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>no</th>
                                    <th>nama alternatif</th>
                                    <th>nilai Q</th>
                                    <th>ranking</th>
                                    <th>kelayakan</th>
                                </tr>
                                </thead>
                               
                                <tbody>
                                    @for ($a=0;$a<$b;$a++)
                                    
                                    <tr>
                                        <td>{{$a+1}}</td>
                                        <td>{{$rankk[$a]}}</td>
                                        <td>{{round($tes[$a],2)}}</td>
                                        <td>{{$a+1}}</td>
                                       
                                        
                                        <td>
                                        {{$kelayakan[$a]}}
                                       </td>
                                       
                                    </tr>

                                    @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('script')
    <script src="{{asset('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/vfs_fonts.jsm')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/js/init/datatables-init.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#bootstrap-data-table-export').DataTable();
            $('body').on('click','.passingId',function(){
               var a =$("#nama_kriteria").html($(this).attr('data-nama_kriteria'));
               $("#nama_subkriteria").val($(this).attr(' data-nama_subkriteria'));
            //   console.log(a.data-nama_kriteria);
        
                
            });
        } );
    </script>
    
    
@endsection
