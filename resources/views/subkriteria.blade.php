@extends('master')
@section('content')
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Sub Kriteria</strong>
                        </div>
                        <div class="card-body">
                            @if(\Illuminate\Support\Facades\Session::has('berhasil'))
                                <div class="alert alert-primary text-black">
                                    <h4>{{\Illuminate\Support\Facades\Session::get('berhasil')}}</h4>
                                </div>
                            @elseif(\Illuminate\Support\Facades\Session::has('gagal'))
                                <div class="alert alert-danger text-black">
                                    <h4>{{\Illuminate\Support\Facades\Session::get('gagal')}}</h4>
                                </div>
                            @endif
                            <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#mediumModal">
                                Tambah Sub Kriteria
                            </button>
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>kode subKriteria</th>
                                    <th>Nama Kriteria</th>
                                    <th>Nama SubKriteria</th>
                                    <th>Bobot</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($subkriteria as $a)
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$a->kode_subkriteria}}</td>
                                            <td>{{$a->kriteria->nama_kriteria}}</td>
                                            <td>{{$a->nama_subkriteria}}</td>
                                            <td>{{$a->bobot}}</td>
                                            
                                            <td>
                                                <a href="{{route('delete.subkriteria',$a->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                <button data-toggle="modal" data-target="#editMediumModal" class="btn btn-primary passingId"
                                                        data-id="{{$a->id}}"
                                                        data-kode_subkriteria = "{{$a->kode_subkriteria}}"
                                                        data-nama_kriteria = "{{$a->kriteria->nama_kriteria}}"
                                                        data-nama_subkriteria = "{{$a->nama_subkriteria}}"
                                                        data-bobot = "{{$a->bobot}}">
                                                    <i class="fa fa-edit"></i></button>
                                            </td>
                                        </tr>
                                      @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->

        <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Tambah Sub Kriteria Baru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="get" action="{{route('store.subkriteria')}}">
                            @csrf
                            <div class="form-group">
                                <label for="">Kode Sub Kriteria</label>
                                <input type="text" name="kode_subkriteria" required class="form-control">
                            </div>
                            <div class="form-group">
                                    <label for="">Nama Kriteria</label>
                                    <select name="kode_kriteria"  class="form-control">
                                      @foreach ($kriteria as $a)
                                            <option value="{{$a->kode_kriteria}}">{{$a->nama_kriteria}}</option>
                                            @endforeach
                                       
                                    </select>
                            </div>
                            <div class="form-group">
                                <label for="">Nama Sub Kriteria</label>
                                <input type="text" name="nama_subkriteria" required class="form-control">
                            </div>
                            
                            <div class="form-group">
                                <label for="">Bobot</label>
                                <input type="text" name="bobot" required class="form-control">
                            </div>
                            <div class="form-group">
                                <button class="btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="editMediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Edit Kriteria</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="get" action="{{route('update.subkriteria')}}">
                            @csrf
                            <div class="form-group">
                                <input type="hidden" name="id" id="id_subkriteria">
                                <label for="">Kode Sub Kriteria</label>
                                 <input type="text" name="kode_subkriteria" id="kode_subkriteria" required class="form-control">
                            </div>
                            <div class="form-group">
                                    <label for="">Nama Krieria</label>
                                    <select name="nama_kriteria" id="nama_kriteria" class="form-control">
                                    @foreach ($kriteria as $a)
                                            <option >{{$a->nama_kriteria}}</option>
                                            @endforeach
                                       
                                    </select>
                            </div>
                            <div class="form-group">
                                <label for="">Nama Sub Kriteria</label>
                                <input type="text" name="nama_subkriteria" id="nama_subkriteria" required class="form-control">
                            </div>
                           
                            <div class="form-group">
                                <label for="">Bobot</label>
                                <input type="text" name="bobot" id="bobot" required class="form-control">
                            </div>
                            <div class="form-group">
                                <button class="btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .content -->
@endsection
@section('script')
    <script src="{{asset('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/vfs_fonts.jsm')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/js/init/datatables-init.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#bootstrap-data-table-export').DataTable();
            $('body').on('click','.passingId',function(){
                $("#kode_subkriteria").val($(this).attr('data-kode_subkriteria'));
                $("#nama_kriteria").val($(this).attr('data-nama_kriteria'));
                $("#nama_subkriteria").val($(this).attr('data-nama_subkriteria'));
                $("#bobot").val($(this).attr('data-bobot'));
                $("#id_subkriteria").val($(this).attr('data-id'));
            });
        } );
    </script>

@endsection
