@extends('master')
@section('content')
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Penilaian</strong>
                        </div>
                        <div class="card-body">
                            @if(\Illuminate\Support\Facades\Session::has('berhasil'))
                                <div class="alert alert-primary text-black">
                                    <h4>{{\Illuminate\Support\Facades\Session::get('berhasil')}}</h4>
                                </div>
                            @elseif(\Illuminate\Support\Facades\Session::has('gagal'))
                                <div class="alert alert-danger text-black">
                                    <h4>{{\Illuminate\Support\Facades\Session::get('gagal')}}</h4>
                                </div>
                            @endif
                            <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#mediumModal">
                                Tambah penilaian
                            </button>
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Alternatif</th>
                                   
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                               
                              
                                @foreach($relasi as $a)
                                    
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$a->alternatif->nama_alternatif}}</td>
                                           
                                          
                                            <td>
                                                <a href="{{route('view.penilaian',$a->kode_alternatif)}}" class="btn btn-warning"><i class="fa fa-eye"></i></a>
                                                <a href="{{route('delete.penilaian',$a->kode_alternatif)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                               
                                            </td>
                                        </tr>
                                        @endforeach
                                       
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->

        <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Tambah Kriteria Baru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="get" action="{{route('store.penilaian')}}">
                            @csrf
                            <div class="form-group">
                                <label for="">alternatif</label>
                                <select name="alternatif"  required class="form-control">
                                    @foreach ($alternatif as $a)
                                    <option value="{{$a->kode_alternatif}}">{{$a->nama_alternatif}}</option>
                                    @endforeach
                                    </select>
                            </div>

                            <div class="form-group">
                                @for ($a=0;$a<$b;$a++)
                                
                
                                    <label for="">{{$kriteria[$a]->nama_kriteria}}</label>
                                    <input type="hidden" name="kriteriaa[]" value="{{$kriteria[$a]->kode_kriteria}}" >
                                    <select name="kriteria[]" id="" class="form-control">
                                        @foreach($v[$a] as $t)
                                       <option value="{{$t}}">{{$t}}</option>
                                   @endforeach
                                 
                                       
                                    </select>
                                    @endfor
                            </div>
                            
                            <div class="form-group">
                                <button class="btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        
    </div><!-- .content -->
@endsection
@section('script')
    <script src="{{asset('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/vfs_fonts.jsm')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/js/init/datatables-init.js')}}"></script>
    
@endsection
