@extends('master')
@section('content')
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">edit penilaian</strong>
                        </div>
                        <div class="card-body">
                            @if(\Illuminate\Support\Facades\Session::has('berhasil'))
                                <div class="alert alert-primary text-black">
                                    <h4>{{\Illuminate\Support\Facades\Session::get('berhasil')}}</h4>
                                </div>
                            @elseif(\Illuminate\Support\Facades\Session::has('gagal'))
                                <div class="alert alert-danger text-black">
                                    <h4>{{\Illuminate\Support\Facades\Session::get('gagal')}}</h4>
                                </div>
                            @endif
                
                          
                            <form method="post" action="{{route('k',$relasi->id)}}">
                            @csrf
                           
                            <div class="form-group">
                            <label for="">{{$kriteria->nama_kriteria}}</label>
                            <input type="hidden" name="a" value="{{$relasi->id}}">
                            <input type="hidden" name="k" value="{{$relasi->kode_kriteria}}">

                          
                            <select name="subkriteria" id="" class="form-control">
                                      @foreach($subkriteria as $s)
                                           <option value="{{$s->kode_subkriteria}}">{{$s->nama_subkriteria}}</option>
                                       @endforeach
                                    </select>
                            </div>
                            
                            <div class="form-group">
                                <button type="submit" class="btn-primary">Simpan 1</button>
                                
                              
                            </div>
                          
                            </form>
                       
                          
                         
                        </div>
                        <a class="btn btn-secondary" href="{{route('view.penilaian',$relasi->kode_alternatif)}}" >cancel</a>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->

        
       
    </div><!-- .content -->
@endsection
@section('script')
    <script src="{{asset('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/vfs_fonts.jsm')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/js/init/datatables-init.js')}}"></script>
  
    
    
@endsection
