@extends('master')
@section('content')
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Alternatif</strong>
                        </div>
                        <div class="card-body">
                            @if(\Illuminate\Support\Facades\Session::has('berhasil'))
                                <div class="alert alert-primary text-black">
                                    <h4>{{\Illuminate\Support\Facades\Session::get('berhasil')}}</h4>
                                </div>
                            @elseif(\Illuminate\Support\Facades\Session::has('gagal'))
                                <div class="alert alert-danger text-black">
                                    <h4>{{\Illuminate\Support\Facades\Session::get('gagal')}}</h4>
                                </div>
                            @endif
                            <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#mediumModal">
                                Tambah Alternatif
                            </button>
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Alternatif</th>
                                    <th>Nama Alternatif</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($alternatif as $a)
                                <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$a->kode_alternatif}}</td>
                                            <td>{{$a->nama_alternatif}}</td>
                                            <td>
                                                <a href="{{route('delete.alternatif',$a->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                <button data-toggle="modal" data-target="#editMediumModal" class="btn btn-primary passingId"
                                                        data-id="{{$a->id}}"
                                                        data-alternatif="{{$a->nama_alternatif}}"
                                                        data-kode_alternatif = "{{$a->kode_alternatif}}">
                                                    <i class="fa fa-edit"></i></button>
                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->

        <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Tambah Alternatif Baru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="get" action="{{route('store.alternatif')}}">
                            @csrf
                            <div class="form-group">
                                <label for="">Kode alternatif</label>
                                <input type="text" name="kode_alternatif" required class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Nama Alternatif</label>
                                <input type="text" name="nama_alternatif" required class="form-control">
                            </div>
                            
                            
                            <div class="form-group">
                                <button class="btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="editMediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Edit Kriteria</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="get" action="{{route('update.alternatif')}}">
                            @csrf
                            <div class="form-group">
                                <input type="hidden" name="id" id="id_alternatif">
                                <label for="">Kode Alternatif</label>
                                 <input type="text" name="kode_alternatif"  id="kode_alternatif" required class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Nama alternatif</label>
                                <input type="text" name="nama_alternatif" id="nama_alternatif" required class="form-control">
                            </div>
                            
                            <div class="form-group">
                                <button class="btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .content -->
@endsection
@section('script')
    <script src="{{asset('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/vfs_fonts.jsm')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/js/init/datatables-init.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#bootstrap-data-table-export').DataTable();
            $('body').on('click','.passingId',function(){
               $("#kode_alternatif").val($(this).attr('data-kode_alternatif'));
                $("#nama_alternatif").val($(this).attr('data-alternatif'));
                $("#id_alternatif").val($(this).attr('data-id'));
            });
        } );
    </script>
@endsection
