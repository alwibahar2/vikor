<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kriteria;
class KriteriaController extends Controller
{
    public function kriteria(){
        $kriteria = kriteria::all();
        return view('kriteria',compact('kriteria'));
    }
    public function store_kriteria(Request $request){
        // dd($request->nama_kriteria);
        $data = array(
            "kode_kriteria" => $request->kode_kriteria,
            "nama_kriteria" => $request->nama_kriteria,
            "atribut" => $request->atribut,
            "bobot" => $request->bobot
        );

        $kriteria = new kriteria();
        $kriteria->fill($data);
        if($kriteria->save()){
            return redirect(route('kriteria'))->with('berhasil','Berhasil Menambahkan kriteria Baru');
        }else{
            return redirect(route('kriteria'))->with('gagal','Gagal Menambahkan kriteria Baru');
        }
    }
    public function update_kriteria(Request $request){
        // dd($request->id);
        $kriteria = kriteria::where("id",$request->id)->first();
        $kriteria->kode_kriteria = $request->kode_kriteria;
        $kriteria->nama_kriteria = $request->nama_kriteria;
        $kriteria->atribut = $request->atribut;
        $kriteria->bobot = $request->bobot;
        // dd($request->bobot/2);
        if($kriteria->save()){
            return redirect(route('kriteria'))->with('berhasil','Berhasil Mengubah kriteria');
        }else{
            return redirect(route('kriteria'))->with('gagal','Gagal Mengubah kriteria');
        }
    }
    public function delete_kriteria($id){
        $deleted = kriteria::where("id",$id)->delete();
        if($deleted){
            return redirect(route('kriteria'))->with('berhasil','Berhasil Menghapus kriteria');
        }else{
            return redirect(route('kriteria'))->with('gagal','Gagal Menghapus kriteria');
        }
    }
}
