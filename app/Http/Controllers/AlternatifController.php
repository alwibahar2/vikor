<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alternatif;

class AlternatifController extends Controller
{
    public function alternatif(){
        $alternatif = alternatif::all();
        return view('alternatif',compact('alternatif'));
    }
    public function store_alternatif(Request $request){
        $data = array(
            "kode_alternatif" => $request->kode_alternatif,
            "nama_alternatif" => $request->nama_alternatif
        );

        $alternatif = new alternatif();
        $alternatif->fill($data);
        if($alternatif->save()){
            return redirect(route('alternatif'))->with('berhasil','Berhasil Menambahkan alternatif Baru');
        }else{
            return redirect(route('alternatif'))->with('gagal','Gagal Menambahkan alternatif Baru');
        }
    }
    public function update_alternatif(Request $request){
        // dd($request->id);
        $alternatif = alternatif::where("id",$request->id)->first();
        $alternatif->kode_alternatif = $request->kode_alternatif;
        $alternatif->nama_alternatif = $request->nama_alternatif;
        if($alternatif->save()){
            return redirect(route('alternatif'))->with('berhasil','Berhasil Mengubah Alternatif');
        }else{
            return redirect(route('alternatif'))->with('gagal','Gagal Mengubah Alternatif');
        }
    }
    public function delete_alternatif($id){
        $deleted = alternatif::where("id",$id)->delete();
        if($deleted){
            return redirect(route('alternatif'))->with('berhasil','Berhasil Menghapus alternatif');
        }else{
            return redirect(route('alternatif'))->with('gagal','Gagal Menghapus alternatif');
        }
    }
   

    //
}
