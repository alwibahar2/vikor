<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kriteria;
use App\Models\Alternatif;
use App\Models\Subkriteria;
use App\Models\Relasi;
use DB;
class PenilaianController extends Controller
{
    public function penilaian(){
        $r=[];
        $kriteria = kriteria::all();
        // select(DB::raw("SUM(transaksirinci.jumlah) as total"),DB::raw("DATE_FORMAT(transaksi.tanggal, '%Y-%m')bulan") )
        //   ->join('transaksirinci','transaksi.id','=','transaksirinci.idPenjualan')
        
        //   ->groupBy(DB::raw("DATE_FORMAT(transaksi.tanggal, '%Y-%m')"))
        //   ->get();

        // $relasi=relasi::select(DB::raw("nama_alternatif.alternatif as "))

        // $relasi=r

        $relasi = relasi::with('alternatif')->select(DB::raw('count(*) as kode_alternatif, kode_alternatif'))
        ->groupBy('kode_alternatif')
        ->get();
        // dd($relasi);
        foreach ($relasi as $key ) {
            // dd('s');
            $r[]=$key->kode_alternatif;
        }
        // dd($relasi);
     $alternatif2 = alternatif::where('kode_alternatif',$r)->get();
       
       
        $alternatif= alternatif::whereNotIn('kode_alternatif',$r)->get();
     
        $b=count($kriteria);
        
    //  dd($kriteria[0]->kode_kriteria);
    $subkriteria=array();
    $v=null;
    
        for ($a=0;$a<$b;$a++){
        $subkriteria = subkriteria::where('kode_kriteria',$kriteria[$a]->kode_kriteria)->get();
        foreach ($subkriteria as $key ) {
            
            $v[$a][]=$key->nama_subkriteria;
        }
        
        }
        for($a=0;$a<$b;$a++)
       
        // dd($v);

        $relasi1=relasi::with('alternatif','kriteria','subkriteria')->get();
// dd($relasi);
        
        return view('penilaian',['kriteria'=>$kriteria,'alternatif'=>$alternatif,'alternatif2'=>$alternatif2,'subkriteria'=>$subkriteria,'v'=>$v,'b'=>$b,'relasi'=>$relasi]);
    }



    public function store_penilaian(Request $request){
        
        // dd($request->kriteria);
        $kriteria = kriteria::all();
        $b=count($kriteria);
        $data=array();
        for($a=0;$a<$b;$a++)
        {
            $subkriteria = subkriteria::where("nama_subkriteria",$request->kriteria[$a])->first();
           array_push($data,array(
            "kode_kriteria" => $request->kriteriaa[$a],
            "kode_subkriteria"=> $subkriteria->kode_subkriteria,
            "kode_alternatif" => $request->alternatif
           )
        );  
        }
        // dd($data);

        
       
        $relasi = new relasi();
        $relasi = relasi::all();
        
        // $g=$relasi1->kode_alternatif;
        
        // $relasi->fill($data);

        if(  relasi::insert($data)){   
            return redirect(route('penilaian'))->with('berhasil','Berhasil Menambahkan penilaian Baru');
        }else{
            return redirect(route('penialaian'))->with('gagal','Gagal Menambahkan penilaian Baru');
        }
        
    }
    public function delete_penilaian($kode_alternatif){
        $deleted = relasi::where("kode_alternatif",$kode_alternatif)->delete();
        if($deleted){
            return redirect(route('penilaian'))->with('berhasil','Berhasil Menghapus penilaian');
        }else{
            return redirect(route('penilaian'))->with('gagal','Gagal Menghapus penilaian');
        }
    }

     
    public function penilaian_view($kode_alternatif)
    {
        $kriteria = kriteria::all();
        $b=count($kriteria);
        for ($a=0;$a<$b;$a++){
            $subkriteria = subkriteria::where('kode_kriteria',$kriteria[$a]->kode_kriteria)->get();
            foreach ($subkriteria as $key ) {
                
                $v[$a][]=$key->nama_subkriteria;
            }
            
            }
     
        $relasi1= relasi::with('subkriteria')->where('kode_alternatif',$kode_alternatif)->get();
        // dd($relasi1);
        $relasi = relasi::with('subkriteria','kriteria')->select(DB::raw('count(*) as kode_kritera, kode_kriteria'),DB::raw('count(*) as kode_subkritera, kode_subkriteria'))
        ->groupBy('kode_kriteria','kode_subkriteria')
        ->get();

        // dd($relasi);
        return view ('penilaian_view',['relasi'=>$relasi,'relasi1'=>$relasi1,'b'=>$b,'v'=>$v,'kriteria'=>$kriteria]);
    }
    public function update_penilaian_view($id){
        $relasi= relasi::with('subkriteria')->where('id',$id)->first();
        // dd( $relasi);
        $kriteria= kriteria::where('kode_kriteria',$relasi->kode_kriteria)->first();
        // dd($kriteria);
        $subkriteria = subkriteria::where('kode_kriteria',$relasi->subkriteria->kode_kriteria)->get();
        return view ('edit_penilaian',['relasi'=>$relasi,'subkriteria'=>$subkriteria,'kriteria'=>$kriteria]);
    }
    public function k(Request $request,$id){
        
       $relasi=relasi::where('id',$id)->first();
     
       $relasi->kode_subkriteria=$request->subkriteria;
        
       ;
       if($relasi->save()){
        
        //    return  redirect()->back()->with('berhasil','Berhasil Mengubah subkriteria');
        return redirect(route('view.penilaian',$relasi->kode_alternatif))->with('berhasil','Berhasil Mengubah subkriteria');
    }else{
        return redirect()->back()->with('gagal','Gagal Mengubah subkriteria');
    }
    // dd();
    // return  redirect()->back() ;
    }




}
