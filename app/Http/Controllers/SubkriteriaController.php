<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kriteria;
use App\Models\Subkriteria;
class SubkriteriaController extends Controller
{
    public function subkriteria(){
        $kriteria = kriteria::all();
        $subkriteria = subkriteria::with('kriteria')->get();
        // dd($subkriteria->kode_subkriteria);
        return view('subkriteria',['kriteria'=>$kriteria,'subkriteria'=>$subkriteria]);
    }
    public function store_subkriteria(Request $request){
        
        $data = array(
            "kode_subkriteria" => $request->kode_subkriteria,
            "kode_kriteria" => $request->kode_kriteria,
            "nama_subkriteria" => $request->nama_subkriteria,
            "bobot" => $request->bobot
        );

        $subkriteria = new subkriteria();
        $subkriteria->fill($data);
        if($subkriteria->save()){
            return redirect(route('subkriteria'))->with('berhasil','Berhasil Menambahkan subkriteria Baru');
        }else{
            return redirect(route('subkriteria'))->with('gagal','Gagal Menambahkan subkriteria Baru');
        }
    }
    public function update_subkriteria(Request $request){
        // dd($request->nama_subkriteria);
        $a=$request->nama_kriteria;
        $kriteria = kriteria :: where("nama_kriteria",$a)->first();
        // dd($request->id);
        $subkriteria = subkriteria::where("id",$request->id)->first();
        // dd($subkriteria);
        $subkriteria->kode_subkriteria = $request->kode_subkriteria;
        $subkriteria->kode_kriteria = $kriteria->kode_kriteria;
        $subkriteria->nama_subkriteria = $request->nama_subkriteria;
        $subkriteria->bobot = $request->bobot;
        // dd($request->kode_subkriteria);
        if($subkriteria->save()){
            return redirect(route('subkriteria'))->with('berhasil','Berhasil Mengubah subkriteria');
        }else{
            return redirect(route('subkriteria'))->with('gagal','Gagal Mengubah subkriteria');
        }
    }
    public function delete_subkriteria($id){
        $deleted = subkriteria::where("id",$id)->delete();
        if($deleted){
            return redirect(route('subkriteria'))->with('berhasil','Berhasil Menghapus subkriteria');
        }else{
            return redirect(route('subkriteria'))->with('gagal','Gagal Menghapus subkriteria');
        }
    }
}
