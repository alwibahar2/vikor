<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class RegisterController extends Controller
{
    public function index(){
        return view('register');
    }

    public function register(Request $request){
        $kode_user = Str::random(6);
        $user = array(
            "username" => $request->username,
            "kode_user" => $kode_user,
            "nama" => $request->nama,
            "nama_orgtua" => $request->nama_orgtua,
            "alamat" => $request->alamat,
            "tempat_lhr" => $request->tempat_lhr,
            "tgl_lhr" => $request->tgl_lhr,
            "status_user" => "0",
            "password" => Hash::make($request->password)
        );
        $new_user = new User();
        $new_user->fill($user);
        if($new_user->save()){
            return redirect('login')->with('berhasil','Berhasil Melakukan Pendaftaran');
        }else{
            return redirect('register')->with('gagal','Gagal Melakukan Pendaftaran');
        }
    }
}
