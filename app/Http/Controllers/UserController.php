<?php

namespace App\Http\Controllers;

use App\Models\Gejala;
use App\Models\HasilDiagnosa;
use App\Models\Penyakit;
use App\Models\RelasiPenyakit;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function index(){
        return view('user.home');
    }

    public function diagnosa(){
        $gejala = Gejala::all();
        return view('user.diagnosa',compact('gejala'));
    }

    public function store_pemeriksaan(Request $request){
        //Ambil data penyakit berdasarkan gejala
        
  
        $penyakit = RelasiPenyakit::where('gejala','like','%'.$request->input('gejala_1').'%')->first()['kode_penyakit'];
            
        

      
        //Pengecheck-an penyakit terhadap rules yang telah disediakan
      
        for($i = 2;$i<=$request->count_gejala;$i++){
            $hasil = RelasiPenyakit::where("kode_penyakit",$penyakit)->where('gejala','like','%'.$request->input('gejala_'.$i).'%')->first();
            // dd($hasil);
            if (!isset($hasil)){
                return redirect(route('diagnosa'))->with('gagal','Penyakit Tidak Ditemukan');
            }
        }
        //Pengambilan nama penyakit berdasarkan kode penyakit
        $nama_penyakit = Penyakit::where("kode_penyakit",$penyakit)->first();
        $hasil_diagnosa = new HasilDiagnosa();
        $data = array(
            "kode_user" => Session::get('users')['kode_user'],
            "kode_penyakit" =>$penyakit,
        );
        //Penginputan ke hasil diagnosa
        $hasil_diagnosa->fill($data);
        $hasil_diagnosa->save();
        //Menampilkan pesan berhasil
        return redirect(route('diagnosa'))->with('berhasil',$nama_penyakit['nama_penyakit']);
    }

    public function ganti_password(){
        return view('user.ganti_password');
    }

    public function change_password(Request $request){
        $user = User::where("kode_user",Session::get('users')['kode_user'])->first();
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect(route('ganti_password_pasien'))->with('berhasil','berhasil mengganti password');
    }

    public function riwayat_diagnosa(){
        $hasil_diagnosa = HasilDiagnosa::with('user','penyakit')->where('kode_user',Session::get('users')['kode_user'])->get();
        return view('user.riwayat_diagnosa',compact('hasil_diagnosa'));
    }
}
