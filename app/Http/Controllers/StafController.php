<?php

namespace App\Http\Controllers;

use App\Models\HasilDiagnosa;
use App\Models\Penyakit;
use App\Models\RelasiPenyakit;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Gejala;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class StafController extends Controller
{
    public function index(){
        $gejala = Gejala::all()->count();
        $penyakit = Penyakit::all()->count();
        $hasil = HasilDiagnosa::all()->count();
        $pasien = User::all()->count();
        $hasil_diagnosa = HasilDiagnosa::with('user','penyakit')->get();

        return view('staf.home',compact('gejala','penyakit','hasil','pasien','hasil_diagnosa'));
    }

    public function pasien_data(){
        $pasien = User::where("status_user",'!=',1)->get();
        return view('staf.pasien',compact('pasien'));
    }

    public function edit_pasien_data(){
        return view('staf_edit_pasien');
    }

    public function hasil_diagnosa_data(){
        $hasil_diagnosa = HasilDiagnosa::with('user','penyakit')->get();
        return view('staf.hasil_diagnosa',compact('hasil_diagnosa'));
    }

    public function penyakit_data(){
        $penyakit = Penyakit::all();
        return view('staf.penyakit',compact('penyakit'));
    }

    public function gejala_data(){
        $gejala = Gejala::all();
       
        return view('staf.gejala',['gejala'=>$gejala]);
    }

    public function relasi_penyakit(){
        $i = 0;
        $gejala_penyakit = Gejala::all();
        $penyakit = Penyakit::all();
        $relasi = RelasiPenyakit::with('penyakit')->get();
        $nama_gejala = array();
        foreach ($relasi as $r){
            foreach (explode(' ',$r->gejala) as $g){
                $gejala = Gejala::where("kode_gejala",$g)->first();
                array_push($nama_gejala,$gejala['nama_gejala']);
            }
            $r->nama_gejala = $nama_gejala;
            $nama_gejala = array();
        }
        return view('staf.relasi',compact('relasi','gejala_penyakit','penyakit'));
    }

    public function store_gejala(Request $request){
        $data = array(
            "kode_gejala" => $request->kode_gejala,
            "nama_gejala" => $request->nama_gejala
        );

        $gejala = new Gejala();
        $gejala->fill($data);
        if($gejala->save()){
            return redirect(route('staf_gejala'))->with('berhasil','Berhasil Menambahkan Gejala Baru');
        }else{
            return redirect(route('staf_gejala'))->with('gagal','Gagal Menambahkan Gejala Baru');
        }
    }
    public function update_gejala(Request $request){
        $gejala = Gejala::where("id",$request->id)->first();
        $gejala->kode_gejala = $request->kode_gejala;
        $gejala->nama_gejala = $request->nama_gejala;
        if($gejala->save()){
            return redirect(route('staf_gejala'))->with('berhasil','Berhasil Mengubah Gejala');
        }else{
            return redirect(route('staf_gejala'))->with('gagal','Gagal Mengubah Gejala');
        }
    }

    public function delete_pasien($id){
        $deleted = Pasien::where("kode_user",$id)->delete();
        if($deleted){
            return redirect(route('staf_pasien'))->with('berhasil','Berhasil Menghapus Pasien');
        }else{
            return redirect(route('staf_pasien'))->with('gagal','Gagal Menghapus Pasien');
        }
    }

    public function delete_gejala($id){
        $deleted = Gejala::where("id",$id)->delete();
        if($deleted){
            return redirect(route('staf_gejala'))->with('berhasil','Berhasil Menghapus Gejala');
        }else{
            return redirect(route('staf_gejala'))->with('gagal','Gagal Menghapus Gejala');
        }
    }

    public function delete_relasi($id){
        $deleted = RelasiPenyakit::where("id",$id)->delete();
        if($deleted){
            return redirect(route('relasi'))->with('berhasil','Berhasil Menghapus Relasi Penyakit');
        }else{
            return redirect(route('relasi'))->with('gagal','Gagal Menghapus Relasi Penyakit');
        }
    }

    public function store_relasi(Request $request){
        $gejala = array();
        for($i = 1;$i<=$request->count_gejala;$i++){
//            $gejala .= $request->gejala_.$i.",";
            array_push($gejala,$request->input('gejala_'.$i));
        }
        $data = array(
          "kode_penyakit" => $request->penyakit,
            "gejala" => implode(" ",$gejala)
        );
        $relasi = new RelasiPenyakit();
        $relasi->fill($data);
        if($relasi->save()){
            return redirect(route('relasi'))->with('berhasil','Berhasil Menambahkan Relasi Penyakit');
        }else{
            return redirect(route('relasi'))->with('gagal','Berhasil Menambahkan Relasi Penyakit');
        }

    }

    public function store_penyakit(Request $request){
        $data = array(
            "kode_penyakit" => $request->kode_penyakit,
            "nama_penyakit" => $request->nama_penyakit,
            "penangan" => $request->penanganan
        );
        $penyakit = new Penyakit();
        $penyakit->fill($data);
        if($penyakit->save()){
            return redirect(route('staf_penyakit'))->with('berhasil','Berhasil Menambahkan Penyakit Baru');
        }else{
            return redirect(route('staf_penyakit'))->with('gagal','Gagal Menambahkan Penyakit Baru');
        }
    }

    public function delete_penyakit($id){
        $deleted = Penyakit::where("id",$id)->delete();
        if($deleted){
            return redirect(route('staf_penyakit'))->with('berhasil','Berhasil Menghapus Penyakit');
        }else{
            return redirect(route('staf_penyakit'))->with('gagal','Gagal Menghapus Penyakit');
        }
    }

    public function update_penyakit(Request $request){
        $penyakit = Penyakit::where("id",$request->id)->first();
        $penyakit->kode_penyakit = $request->kode_penyakit;
        $penyakit->nama_penyakit = $request->nama_penyakit;
        $penyakit->penangan = $request->penanganan;
        if($penyakit->save()){
            return redirect(route('staf_penyakit'))->with('berhasil','Berhasil Mengubah Penyakit');
        }else{
            return redirect(route('staf_penyakit'))->with('gagal','Gagal Mengubah Penyakit');
        }
    }

    public function ganti_password(){
        return view('staf.ganti_password');
    }

    public function change_password(Request $request){
        $user = User::where("kode_user",Session::get('users')['kode_user'])->first();
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect(route('ganti_password_staf'))->with('berhasil','berhasil mengganti password');
    }

}
