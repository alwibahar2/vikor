<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kriteria;
use App\Models\Alternatif;
use App\Models\Subkriteria;
use App\Models\Relasi;
use DB;

class PerhitunganController extends Controller
{
   public function perhitungan (){
      $kriteria= kriteria::all();
     
      $n=count($kriteria)+1;
      // dd($kriteria);
      $relasi1 = relasi::with('alternatif')->select(DB::raw('count(*) as kode_alternatif, kode_alternatif'))
      ->groupBy('kode_alternatif')
      ->get();
   
      $relasi2 = relasi::with('alternatif','subkriteria')->select(DB::raw('count(*) as kode_alternatif, kode_alternatif'),DB::raw('count(*) as kode_subkritera, kode_subkriteria'))
      ->groupBy('kode_alternatif','kode_subkriteria')
      ->get();
      $relasi = relasi::with('subkriteria','kriteria','alternatif')->select(DB::raw('count(*) as kode_kritera, kode_kriteria'),DB::raw('count(*) as kode_subkritera, kode_subkriteria'),DB::raw('count(*) as kode_alternatif, kode_alternatif'))
      ->groupBy('kode_kriteria','kode_subkriteria','kode_alternatif')
      ->get();
      $atribut=[];
      $bobot=[];
      $c=[];
      $d=[];
      $nilai=[];
      $max=[];
      $maxx=[];
      $maxxx=[];
      $minnn=[];
      $min=[];
      $niali1=[];
      $w=[];
      $normalisasi=[];
      $normalisasi1=[];
      $ba=count($kriteria);
       $b=count($relasi1);
       foreach($kriteria as $key){
         $atribut[]=$key->atribut;
         $bobot[]=$key->bobot;
       }
       $bobot1=array_sum($bobot);
   
   foreach($bobot as $key){
         $w[]=$key/$bobot1;
   }
   

       foreach($relasi1 as $a){
         
            array_push($c,$a->alternatif->nama_alternatif);
          
         }
// tabel perhitungan
       for($a=0;$a<$b;$a++)
       {
          
         $d[$a][]=$c[$a];
         $hh=alternatif::where('nama_alternatif',$d[$a])->first();
         $re=relasi::with('subkriteria')->where('kode_alternatif',$hh->kode_alternatif)->get();
         foreach ($re as $key){
            
            $d[$a][]=$key->subkriteria->bobot;

            $nilai[$a][]=$key->subkriteria->bobot;
           

         }   
      //   ( $max($nilai[$a]));       
       }
       for ($a=0;$a<$b;$a++){
         
         for ($ab=0;$ab<$ba;$ab++){
            
             $max[$ab][$a]= $nilai[$a][$ab];
         }
         // $maxx[]=(max($max));
         }
         $maxxx=[];
         $minnn=[];
         $maxx=[];
         $minn=[];
     
      for($a=0;$a<$ba;$a++){
         $maxx=[];
         $minn=[];
         for($i=0;$i<$b;$i++){
            array_push($maxx,$max[$a][$i]);
            array_push($minn,$max[$a][$i]);
         }
if (!empty($maxx))
{         $maxxx[]=(max($maxx)); 
         $minnn[]=(min($minn)); 
}
      }
      
      
   
      // dd($maxxx);
      if (!empty($nilai)){
       $nialimax=(max($nilai));
       $nialimin=(min($nilai));}
    
      $min=[];
      $max=[];
      $maxx=[];
      if (!empty($nilaimin)){
      foreach ($nialimin as $f){
         array_push($min,$f);
      }}
      if (!empty($nilaimax)){
      foreach ($nialimax as $f){
         array_push($max,$f);
      }}
      // dd($max);
   
      

      for($a=0;$a<$b;$a++){

          for ($ab=0;$ab<$ba;$ab++){
             $kondisi=$atribut[$ab];
             if($kondisi !="cost"){
            
                
            $normalisasi[$a][]=((($nialimax[$ab]-$nilai[$a][$ab])==0)?0:($nialimax[$ab]-$nilai[$a][$ab])/($maxxx[$ab]-$minnn[$ab]));
            
             }
             else{
               $normalisasi[$a][]=($nilai[$a][$ab]-$minnn[$ab])/($maxxx[$ab]-$minnn[$ab]);
             }
         }
      }  
      // dd($normalisasi);
      for ($a=0;$a<$b;$a++){
         for($ab=0;$ab<$ba;$ab++){
            $normalisasi1[$a][$ab]=$normalisasi[$a][$ab]*(round($w[$ab],2));
         }

      }

      $s=[];
      $ss=[];
      $r=[];
      for ($a=0;$a<$b;$a++){
         $ss=[];
         
         for($ab=0;$ab<$ba;$ab++){
            array_push($ss,$normalisasi1[$a][$ab]);
         }
         $s[]=(array_sum($ss));
         $r[]=(max($ss));


      }
      $sr=[];
      for($a=0;$a<$b;$a++){
        
           $sr[$a][1]=$s[$a]; 
           $sr[$a][2]=$r[$a]; 
      }
      $srmax=[];
      $srn=[];
      $srmin=[];
      for($a=0;$a<$b;$a++){
         $srn=[];
         for($i=1;$i<3;$i++){
            $sri[$i][$a]=$sr[$a][$i];
         }
      }
      for($a=1;$a<3;$a++){
         $srn=[];
         for($i=0;$i<$b;$i++){
            array_push($srn,$sri[$a][$i]);
         }
         if (!empty($srn)){
         $srmax[]=(max($srn));
         $srmin[]=(min($srn));
      }}
      
      // dd($srmax);
      $qi=[];
       for($a=0;$a<$b;$a++){
          $qi[$a]=((($s[$a]-$srmin[0])/($srmax[0]-$srmin[0]))*0.5)+((($r[$a]-$srmin[1])/($srmax[1]-$srmin[1]))*0.5);
       }
      
      // for($a=0;$a<$b;$a++){
      //    $ss=$qi[$a];
        
      //    if (){
      //       $aa=$a+1;
      //    }        

      // } 
      // dd(array_unique($qi));
      // dd(sort($qi, SORT_NUMERIC));
         $rank=[];
         for($a=0;$a<$b;$a++){
            $t=$relasi1[$a]->alternatif->nama_alternatif;
          
              $rank[$a][0]=$t;
              $rank[$a][1]=round($qi[$a],2);
           
         }
// dd($rank);

      //  $xx=(rsort($qi));
      //  dd($qi);
      $tes=[];
      $ii=$qi;
      $i=$b;
      foreach($ii as $key)
      {
          $max = min($ii);
          $tes[]=$max;
          $keys = array_search($max, $ii);    
          unset($ii[$keys]);
          if(sizeof($ii) >0)
          if(!in_array($max,$ii))
              $i--;
      
      }
      // $vv=[1,2];
      // if (in_array(2,$vv)){
      //    dd("a");
      // }
      // $tess=array_unique($tes);
   $rankk=[];
         for ($a=0;$a<$b;$a++){
            $z=round($tes[$a],2);
            for ($c=0;$c<$b;$c++){
               $y=$rank[$c][1];
               if (in_array($rank[$c][0],$rankk)){

               }
          
              else if ($z == $y  ){
               //   dd($rank[$c]);
                 array_push($rankk,$rank[$c][0]);
              }

            }
         }
         // dd($rankk);
      //  dd( array_unique($tes));

   //  dd($tes);
   // $rank       = 0; 
   // $hiddenrank = 0;
   // $hold = null;
   // foreach ( $ii as $key=>$val ) {
   //     # Always increade hidden rank
   //     $hiddenrank += 1;
   //     # If current value is lower than previous:
   //     # set new hold, and set rank to hiddenrank.
   //     if ( is_null($hold) || $val < $hold ) {
   //         $rank = $hiddenrank; $hold = $val;
   //     }    
   //     # Set rank $rank for $in[$key]
   //     $in[$key] = $rank;
   // } 
   // dd($in); 
     
   
  $dua=2;
  $layak=100;
  $kondisi=kriteria::all();
  foreach ($kondisi as $set){

   if($set->bobot <$layak){
      $layak=$set->kode_kriteria;
   }
  }
//   dd($layak);
  $kelayakan1=relasi::where('kode_kriteria',$layak)->get();
//   dd($kelayakan1);
  $kelayakan2;
  foreach($kelayakan1 as $key){
// dd($key->kode_subkriteria);
$kelayakan2[]=$key->kode_subkriteria;
  }
$kelayakan=[];
// dd($kelayakan2);
foreach($kelayakan2 as $key){
   $kelayakan3=subkriteria::where('kode_subkriteria',$key)->first();
   // dd($tes);
   if($kelayakan3->bobot==$minnn[0]){

            $kelayakan[]='tidak layak';
         }else{
            $kelayakan[]='layak';
      
         }
   // $kelayakan[]=$kelayakan3->bobot;
}
// dd($kelayakan);
// dd($minnn);
//   $kelayakan3=subkriteria::whereIn('kode_subkriteria',$kelayakan2)->get();
//   foreach($kelayakan3 as $key){
//    
//   }
//   dd($kelayakan);
// $lyk=[];epe

    return view('perhitungan',['kriteria'=>$kriteria,'d'=>$d,'maxxx'=>$maxxx,'minnn'=>$minnn,'b'=>$b,'c'=>$c,'n'=>$n,'nilai'=>$nilai,
    'normalisasi'=>$normalisasi,'relasi'=>$relasi1,'bobot'=>$bobot,'w'=>$w,'normalisasi1'=>$normalisasi1,'s'=>$s,'r'=>$r,'sr'=>$sr,
    'dua'=>$dua,'srmax'=>$srmax,'srmin'=>$srmin,'qi'=>$qi,'rankk'=>$rankk,'tes'=>$tes,'kelayakan'=>$kelayakan]);
   }
}
