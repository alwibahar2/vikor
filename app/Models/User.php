<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $primaryKey = 'id';
    protected $table = 'user';
    public $timestamps = false;
    protected $fillable = [
        'username',
        'password',
        'nama',
        'kode_user',
        'nama_orgtua',
        'alamat',
        "status_user",
        'tempat_lhr',
        'tgl_lhr'
    ];
}
