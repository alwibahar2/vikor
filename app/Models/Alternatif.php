<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alternatif extends Model
{
    use HasFactory;
    public $primaryKey = 'id';
    protected $table = 'alternatif';
    public $timestamps = false;
    protected $fillable = [
        'kode_alternatif',
        'nama_alternatif',
    ];
    public function Relasi()
    {
        return $this->hasMany(Relasi::class);
    }
}
