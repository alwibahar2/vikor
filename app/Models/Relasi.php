<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Relasi extends Model
{
    public $primaryKey = 'id';
    protected $table = 'relasi';
    protected $guarded = ['id']; 
    public $timestamps = false;


    public function Alternatif ()
    {
        return $this->belongsTo(Alternatif::class,'kode_alternatif','kode_alternatif');
    }

    public function Kriteria ()
    {
        return $this->belongsTo(Kriteria::class,'kode_kriteria','kode_kriteria');
    }
    public function Subkriteria ()
    {
        return $this->belongsTo(subKriteria::class,'kode_subkriteria','kode_subkriteria');
    }
    
}
