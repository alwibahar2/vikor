<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kriteria extends Model
{
    use HasFactory;
    public $primaryKey = 'id';
    protected $table = 'kriteria';
    protected $guarded = ['id']; 
    public $timestamps = false;
    protected $fillable = [
        'kode_kriteria',
        'nama_kriteria',
        'atribut',
        'bobot'
    ];
    public function Subkriteria()
    {
        return $this->hasMany(Subkriteria::class);
    }
    public function Relasi()
    {
        return $this->hasMany(Relasi::class);
    }
}
