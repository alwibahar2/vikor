<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subkriteria extends Model
{
    public $primaryKey = 'id';
    protected $table = 'subkriteria';
    protected $guarded = ['id']; 
    protected $fillable = [
        'kode_subkriteria',
        'kode_kriteria',
        'nama_subkriteria',
        'bobot'
    ];
    public $timestamps = false;

    public function Kriteria ()
    {
        return $this->belongsTo(Kriteria::class,'kode_kriteria','kode_kriteria');
    }

    public function Relasi()
    {
        return $this->hasOne(Relasi::class);
    }
}
