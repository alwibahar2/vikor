<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::middleware('admin_check')->group(function (){


Route::get('/login','LoginController@index')->name('login');
Route::get('/logout','LoginController@logout')->name('logout');
Route::get('/register','RegisterController@index')->name('register');
Route::post('/store_register','RegisterController@register')->name('store_register');
Route::post('/verify','LoginController@verify')->name('verify');
Route::post('/store_register','RegisterController@register')->name('store_register');
Route::get('/register_dummy','LoginController@register_dummy')->name('register_dummy');

Route::get('/home','AdminController@index')->name('home');


Route::get('/kriteria','KriteriaController@kriteria')->name('kriteria');
Route::get('/kriteria/store','KriteriaController@store_kriteria')->name('store.kriteria');
Route::get('/kriteria/update','KriteriaController@update_kriteria')->name('update.kriteria');
Route::get('/kriteria/delete/{id}','KriteriaController@delete_kriteria')->name('delete.kriteria');

Route::get('/subkriteria','SubkriteriaController@subkriteria')->name('subkriteria');
Route::get('/subkriteria/store','SubkriteriaController@store_subkriteria')->name('store.subkriteria');
Route::get('/subkriteria/update','SubkriteriaController@update_subkriteria')->name('update.subkriteria');
Route::get('/subkriteria/delete/{id}','SubkriteriaController@delete_subkriteria')->name('delete.subkriteria');

Route::get('/alternatif','AlternatifController@alternatif')->name('alternatif');
Route::get('/alternatif/store','AlternatifController@store_alternatif')->name('store.alternatif');
Route::get('/alternatif/update','AlternatifController@update_alternatif')->name('update.alternatif');
Route::get('/alternatif/delete/{id}','AlternatifController@delete_alternatif')->name('delete.alternatif');

Route::get('/penilaian','PenilaianController@penilaian')->name('penilaian');
Route::get('/penilaian/store','PenilaianController@store_penilaian')->name('store.penilaian');
Route::get('/penilaian/delete/{kode_alternatif}','PenilaianController@delete_penilaian')->name('delete.penilaian');
Route::get('/penilaian/view/{kode_alternatif}','PenilaianController@penilaian_view')->name('view.penilaian');
Route::get('/penilaian/view/update/{id}','PenilaianController@update_penilaian_view')->name('update.view.penilaian');
Route::post('/penilaian/view/k/{id}','PenilaianController@k')->name('k');


Route::get('/perhitungan','PerhitunganController@perhitungan')->name('perhitungan');
});
Route::get('/','LoginController@index')->name('login');
Route::get('/login','LoginController@index')->name('login');
Route::get('/logout','LoginController@logout')->name('logout');
Route::get('/register','RegisterController@index')->name('register');
Route::post('/store_register','RegisterController@register')->name('store_register');
Route::post('/verify','LoginController@verify')->name('verify');

Route::post('/store_register','RegisterController@register')->name('store_register');
Route::get('/register_dummy','LoginController@register_dummy')->name('register_dummy');